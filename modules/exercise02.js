const express = require('express');

const app = express();

app.use((req, res, next) => {
  console.log('first middleware, runs always');
  next();
});

app.use('/users', (req, res, next) => {
  res.send('<h1>Users response:</h1>')
});

app.use('/', (req, res, next) => {
  console.log('second middleware and response');
  res.send('<h1>Second middleware response</h1>');
});

app.listen(3000);


